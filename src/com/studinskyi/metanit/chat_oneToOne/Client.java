package com.studinskyi.metanit.chat_oneToOne;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Client {
    private volatile boolean stopClient = false;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1", 7777);
            OutputStream outputStream = socket.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

            Client client = new Client();

            // старт потока на чтение приходящих с сервера сообщений
            new ClientThread(socket, client);

            String messageFromClient = ""; // динамическая часть, которую клиет вводит с консоли
            // ожидание ввода пользователем клиента чата с консоли, до того момента как пользователь наберёт exit
            while (!messageFromClient.equals("exit")) {
                //headlineMessage = "" + getCurrentDateTimeString() + " client write: ";
                ///System.out.print(getCurrentDateTimeString() + " client write: ");
                Scanner scanner = new Scanner(System.in);
                messageFromClient = scanner.nextLine();
                // отправка сообщения от клиента в сокет
                bufferedWriter.write(messageFromClient + "\n");
                //bufferedWriter.write(messageFromClient);
                bufferedWriter.flush();
            }
            // остановка работы клиента (выставление флага stopClient)
            client.setStopClient(true);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //socket.close();

        //        Socket socket = new Socket("127.0.0.1", 7777);
        //        OutputStream outputStream = socket.getOutputStream();
        //        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        //        dataOutputStream.writeUTF("сообщение из сети , момент сообщения " + getCurrentDateTimeString());
        //        dataOutputStream.flush();
    }

    public boolean isStopClient() {
        return stopClient;
    }

    public void setStopClient(boolean isStop) {
        this.stopClient = isStop;
    }

    public static String getCurrentDateTimeString() {
        Date d = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatDate.format(d);
    }

}

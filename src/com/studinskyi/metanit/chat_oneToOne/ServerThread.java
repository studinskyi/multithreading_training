package com.studinskyi.metanit.chat_oneToOne;

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class ServerThread extends Thread {
    private Socket socket;
    private Server server;

    public ServerThread(Socket socketServer, Server server) {
        this.socket = socketServer;
        this.server = server;
        start();
    }

    @Override
    public void run() {
        try {
            InputStream inpStream = socket.getInputStream();
            waitResponceServer(socket, inpStream);
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println(e.getMessage());
            return;
        }
    }

    public void waitResponceServer(Socket socket, InputStream inpStream) {
        BufferedReader bufReader = new BufferedReader(new InputStreamReader(inpStream));

        String line;
        while (true) {
            try {
                line = bufReader.readLine();
                if (!line.equals(""))
                    System.out.println(getCurrentDateTimeString() + " сервер прочел сообщение : " + line);
                // если клиент ввел в консоль exit, то необходимо прекратить работу сокета
                if (line.equalsIgnoreCase("exit")) {
                    //if ((line == null)
                    socket.close();
                    return;
                } else {
                    // отправка сообщений всем клиентским сокетам
                    if (!line.equals(""))
                        sendAllClientsMessage(line);
                }
            } catch (IOException e) {
                System.out.println("Ошибка чтения сервером информации из сокетов. Работа сервера была прекращена.");
                System.out.println(e.getMessage());
                //e.printStackTrace();
                return;
            }
        }
    }

    public void sendAllClientsMessage(String line) {
        DataOutputStream dataOutStream;

        Set<Socket> setSocketsClients = server.getSetSocket();
        for (Socket socketClient : setSocketsClients) {
            try {
                dataOutStream = new DataOutputStream(socketClient.getOutputStream());
                //line = line + " (сообщение от абонента " + socketClient.getPort() +")";
                dataOutStream.writeUTF(line);
                //dataOutStream.writeBytes(line + "\n\r");
                dataOutStream.flush();
            } catch (IOException e) {
                System.out.println("ошибка отправки сообщения с сервера: " + e.getMessage());
                //e.printStackTrace();
                //return;
            }

        }
    }

    public static String getCurrentDateTimeString() {
        Date d = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatDate.format(d);
    }
}

package com.studinskyi.metanit.chat_oneToOne;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ClientThread extends Thread {
    private Socket socket;
    private Client client;

    public ClientThread(Socket socketClient, Client client) {
        this.socket = socketClient;
        this.client = client;
        start();
    }

    @Override
    public void run() {
        try {
            InputStream inpStream = socket.getInputStream();
            waitOutputServer(inpStream);
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println(e.getMessage());
            return;
        }
    }

    public void waitOutputServer(InputStream inpStream) {

        DataInputStream dataIn = new DataInputStream(inpStream);
        String line = "";
        while (true && !client.isStopClient()) {
            try {
                line = dataIn.readUTF();

            } catch (IOException e) {
                System.out.println("Ошибка чтения клиентом данных из потока.");
                System.out.println(e.getMessage());
                //e.printStackTrace();
            }

            if (!line.equals(""))
                System.out.println("\n" + getCurrentDateTimeString() + " пришло сообщение : " + line);
        }
    }

    public static String getCurrentDateTimeString() {
        Date d = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        return formatDate.format(d);
    }
}

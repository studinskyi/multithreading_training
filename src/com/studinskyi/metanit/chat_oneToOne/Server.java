package com.studinskyi.metanit.chat_oneToOne;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class Server {
    private CopyOnWriteArraySet<Socket> setSocket;
    private List<ServerThread> listServerThreads;

    public Server() {
        this.setSocket = new CopyOnWriteArraySet<>();
        this.listServerThreads = new ArrayList<>();
    }

    public static void main(String[] args) throws IOException {
        Server serverMain = new Server();
        ServerSocket serverSocket = new ServerSocket(7777);
        Socket socket;

        while (true) {
            socket = serverSocket.accept();
            if (serverMain.getSetSocket().contains(socket))
                continue; // в случае наличия этого сокета в пуле соединений, нет смысла его добавлять снова

            System.out.println("connect new client socket: InetAddress = " + socket.getInetAddress()
                    + ", Port = " + socket.getPort()
                    + ", LocalAddress = " + socket.getLocalAddress()
                    + ", getLocalPort = " + socket.getLocalPort()
                    + ", LocalSocketAddress = " + socket.getLocalSocketAddress());
            // сохранение сокета в сете setSocket, для дальнейшей рассылки сообщений клиентам
            serverMain.getSetSocket().add(socket);
            // запуск потока ServerThread для работы с сокетом клиента, а также добавление объекта этого потока в listServerThreads
            serverMain.getListServerThreads().add(new ServerThread(socket, serverMain));
        }
    }

    public Set<Socket> getSetSocket() {
        return setSocket;
    }

    public List<ServerThread> getListServerThreads() {
        return listServerThreads;
    }
}

package com.studinskyi.metanit.hronometr;

public class Clock extends Thread {
    public static volatile boolean isStopped = false;

    private static volatile int timeWork; // количество секунд с момента начала работы программы
    private Boolean itIsClock = false;
    private int periodShowTime = 1;
    private Clock chronometer;

    public Clock() {
        // констуктор для объекта режима хронометра
        this.chronometer = this;
        start();
    }

    public Clock(Boolean itIsClock, int periodShowTime, Clock chronometer) {
        // конструктор для часов
        this.itIsClock = itIsClock;
        this.periodShowTime = periodShowTime;
        this.chronometer = chronometer;
        start();
    }

    public synchronized void addTime() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Поток экземпляра хронометра Clock был прерван по InterruptedException!");
        }
        timeWork = timeWork + 1;
        this.notifyAll();
    }

    public synchronized void showClock(int periodShowTime) {
        if (timeWork % periodShowTime == 0 && timeWork > 0)
            System.out.println("поток " + Thread.currentThread().getName() + " : отсчет периодичности  " + periodShowTime + " текущая секунда" + timeWork);

        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Поток экземпляра часов Clock был прерван по InterruptedException!");
        }
    }

    public void run() {
        while (!Clock.isStopped) {
            if (!itIsClock) {
                chronometer.addTime();
                System.out.println("поток " + Thread.currentThread().getName() + " : хронометр " + timeWork);
            }
            if (itIsClock)
                chronometer.showClock(periodShowTime);

            // тестовое ограничение времени работы таймера, равное 22-м секундам
            if (timeWork >= 22)
                Clock.isStopped = true;
        }
    }
}

package com.studinskyi.metanit.semaphore_cubator;

public class Prostator extends Thread{
    int zArg;
    private String name; // название потока
    MySemaphore mySemaphore; // объект для синхронизации

    public Prostator(int numberThred, int zArg, MySemaphore mySemaphore) {
        this.zArg = zArg;
        this.name = "Prostator " + numberThred;
        this.mySemaphore = mySemaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            mySemaphore.save(zArg);
            System.out.println("поток " + this.name + " : обсчет значения Z = " + zArg + " результат расчет Z = " + zArg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.studinskyi.metanit.semaphore_cubator;

public class Cubator extends Thread {
    int xArg;
    private String name; // название потока
    MySemaphore mySemaphore; // объект для синхронизации

    public Cubator(int numberThred, int xArg, MySemaphore mySemaphore) {
        this.xArg = xArg;
        this.name = "Cubator " + numberThred;
        this.mySemaphore = mySemaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            int currentResult = xArg*xArg*xArg;
            mySemaphore.save(currentResult);
            System.out.println("поток " + this.name + " : обсчет значения X = " + xArg + " результат расчет X3 = " + currentResult);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

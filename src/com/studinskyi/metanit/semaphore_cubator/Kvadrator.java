package com.studinskyi.metanit.semaphore_cubator;

public class Kvadrator extends Thread{
    int yArg;
    private String name; // название потока
    MySemaphore mySemaphore; // объект для синхронизации

    public Kvadrator(int numberThred, int yArg, MySemaphore mySemaphore) {
        this.yArg = yArg;
        this.name = "Kvadrator " + numberThred;
        this.mySemaphore = mySemaphore;
        start();
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
            int currentResult = yArg*yArg;
            mySemaphore.save(currentResult);
            System.out.println("поток " + this.name + " : обсчет значения Y = " + yArg + " результат расчет Y2 = " + currentResult);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

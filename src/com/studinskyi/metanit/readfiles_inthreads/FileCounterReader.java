package com.studinskyi.metanit.readfiles_inthreads;

import java.io.*;

public class FileCounterReader implements Runnable {
    private String fullPathToFile;
    private File file;
    private static volatile int countPositiveNumbers = 0;
    private static volatile boolean stopCountingProcess = false;
    //private PoolCounters poolCounters;

    public FileCounterReader(String fullPathToFile) {
        this.fullPathToFile = fullPathToFile;
        //this.poolCounters = poolResources;
    }

    public void run() {
        try {
            int sumNumbersOfFile = countNumbersFromFile();
            System.out.println(Thread.currentThread().getName() + " : in the file " + fullPathToFile + " the sum of postitive nambers is = " + sumNumbersOfFile);
            //            // удаление обсчитанного файла из списка подготовленных к обсчету
            //            // вариант контроля завершения всех потоков чтения без использования .join для потоков
            //            // для варианта без использования ExecutorService
            //            poolCounters.getListFiles().remove(file);
        } catch (IOException e) {
            System.out.println("error the operation run() for the thread: " + Thread.currentThread());
            e.printStackTrace();
        }
    }

    // синхронизированный метод для суммирования чисел из всех потоков в статическую переменную countPositiveNumbers
    public synchronized static void addPositiveNumber(int positiveNumber) {
        countPositiveNumbers = countPositiveNumbers + positiveNumber;
    }

    public int countNumbersFromFile() throws IOException {
        String strLine;
        String[] stringFragments;
        int tekNumb;
        int sumNumbers = 0;

        file = new File(fullPathToFile);
        if (!file.exists()) {
            System.out.println("file does not exist: " + fullPathToFile);
            return 0; // процесс обсчета отсутствующего файла прекращаем
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fullPathToFile));
            while ((strLine = reader.readLine()) != null && !stopCountingProcess) {
                stringFragments = strLine.split(" "); // разбиенте строки по разделителю в виде пробела
                for (String strFragment : stringFragments) {
                    strFragment = strFragment.replaceAll(" ", ""); // убираем все оставшиеся пробелы во фрагменте
                    if (strFragment.equals("")) // отсекаем случаи когда во фрагменте все пробелы (чтобы не получить при парсинге числа через parseInt ложные ошибки)
                        continue; // переходим к обработке следующего фрагмента в строке
                    if (stopCountingProcess) // если в другом потоке был выставлен флаг остановки, то следует прекратить обсчет и текущего потока
                        break; // завершаем проход по фрагментам строки

                    try {
                        // выделение числа из фрагмента строки и его накопление в общем для потоков ресурсе
                        //System.out.println("read fragment of text " + strFragment + " readig file " + fullPathToFile);
                        tekNumb = Integer.parseInt(strFragment);
                        if (tekNumb > 0 && tekNumb % 2 == 0) {
                            // накапливается сумма только положительных четных чисел
                            addPositiveNumber(tekNumb);
                            sumNumbers = sumNumbers + tekNumb;
                            System.out.println(Thread.currentThread().getName() + " : current sum of numbers: " + countPositiveNumbers);
                            //System.out.println(Thread.currentThread().getName() + " : current sum of numbers: " + countPositiveNumbers + " in moment " + getCurrentDateTimeString() + " in file " + fullPathToFile);
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("ERROR OF PARSE fragment " + strFragment + " from the readig file " + fullPathToFile);
                        stopCountingProcess = true;
                        break; // завершаем проход по фрагментам строки
                        //e.printStackTrace();
                    }
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("error opening file - FileNotFoundException, from path:" + fullPathToFile);
            //e.printStackTrace();
        } catch (IOException e) {
            System.out.println("error opening file - IOException, from path:" + fullPathToFile);
            //e.printStackTrace();
        }
        return sumNumbers; // сумма положительных чисел в данном файла (не относится к основной задаче)
    }

    public static int getCountPositiveNumbers() {
        return countPositiveNumbers;
    }

    public static void setCountPositiveNumbers(int countPositiveNumbers) {
        FileCounterReader.countPositiveNumbers = countPositiveNumbers;
    }

    public static boolean isStopCountingProcess() {
        return stopCountingProcess;
    }
}

package com.studinskyi.metanit.readfiles_inthreads;

public class Main {
    public static void main(String[] args) {
        String fullPathDirectory = ProjectUtilits.setWorkFolder("c:\\test_QA\\");
        // открытие пула потоков и запуск потоков на чтение файлов
        PoolCounters poolCounters = new PoolCounters(fullPathDirectory);
        poolCounters.runAndSummingNumbersFromTreads();
        System.out.println("Total sum of positive numbers =  " + poolCounters.getSumNumbersOfPool());
    }
}

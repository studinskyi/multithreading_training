package com.studinskyi.metanit.readfiles_inthreads;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolCounters {
    private String fullPathDirectory = "";
    private List<File> listFiles;

    public PoolCounters(String fullPathDirectory) {
        this.fullPathDirectory = fullPathDirectory;
    }

    public void runAndSummingNumbersFromTreads() {
        listFiles = ProjectUtilits.listFilesInDirectory(fullPathDirectory);// получение списка путей к файлам из указанной папки
        System.out.println("Directory contains " + listFiles.size() + " files");
        int countFiles = 0;
        // запуск потоков через ExecutorService чтобы ограничить одновременную работу до 2 потоков
        ExecutorService serviceExecutor = Executors.newFixedThreadPool(2);
        for (File elemFile : listFiles) {
            // запуск чтения и подсчета каждого файла в отдельном объекте потока Thread
            //readResource(elemFile, ++countFiles); // для варианта без использования ExecutorService
            readResource(elemFile, ++countFiles, serviceExecutor);
        }

        serviceExecutor.shutdown();
        //        try {
        //            serviceExecutor.awaitTermination(0, TimeUnit.SECONDS);
        //        } catch (InterruptedException e) {
        //            e.printStackTrace();
        //        }
        while (!serviceExecutor.isTerminated()) {
            System.out.println("ожидание завершения ExecutorService " + ProjectUtilits.getCurrentDateTimeString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //            // попытка остановить ВСЕ потоки через экзекьютор сразу при появлении
            //            // в одном из потоков чтения нежелательного символа и выставлении флага stopCountingProcess
            //            if (FileCounterReader.isStopCountingProcess())
            //                serviceExecutor.shutdownNow(); //
        }

        //                // попытка дождаться завершения чтения всех открытых в потоках файлов
        //                // вариант контроля завершения всех потоков чтения без использования .join для потоков
        //                // для варианта без использования ExecutorService
        //                try {
        //                    while (true) {
        //                        if (listFiles.size() == 0)
        //                            break;
        //                        TimeUnit.MILLISECONDS.sleep(1000);
        //                        //Thread.sleep(500);
        //                    }
        //                } catch (InterruptedException e) {
        //                }

    }

    public void readResource(File elemResource, int numberOfResource, ExecutorService serviceExecutor) {
        // для варианта с использованием ExecutorService
        System.out.println("start counting process in file " + numberOfResource + ": " + elemResource.getPath());
        FileCounterReader counterReader = new FileCounterReader(elemResource.getPath());
        Runnable runnableThread = new Thread(counterReader, "thread " + numberOfResource);
        serviceExecutor.execute(runnableThread);
    }

    private void readResource(File elemResource, int numberOfResource) {
        // для варианта без использования ExecutorService
        // запуск ВСЕХ потоков параллельно
        System.out.println("start counting process in file " + numberOfResource + ": " + elemResource.getPath());
        FileCounterReader counterReader = new FileCounterReader(elemResource.getPath());
        Thread thread = new Thread(counterReader, "thread " + numberOfResource);
        thread.start();
    }

    public Integer getSumNumbersOfPool() {
        return FileCounterReader.getCountPositiveNumbers();
    }

    public List<File> getListFiles() {
        return listFiles;
    }
}

package com.studinskyi.metanit.exchanger_messages;

import java.util.concurrent.Exchanger;

class GetThread implements Runnable {

    Exchanger<String> exchanger;
    String message;

    GetThread(Exchanger ex) {

        this.exchanger = ex;
        message = "Привет мир!";
    }

    public void run() {

        try {
            message = exchanger.exchange(message);
            System.out.println("GetThread получил: " + message);
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

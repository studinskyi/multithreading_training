package com.studinskyi.metanit.exchanger_messages;

import java.util.concurrent.Exchanger;

public class ThreadsApp {

    public static void main(String[] args) {

        Exchanger<String> ex = new Exchanger();
        new Thread(new PutThread(ex)).start();
        new Thread(new GetThread(ex)).start();
    }
}

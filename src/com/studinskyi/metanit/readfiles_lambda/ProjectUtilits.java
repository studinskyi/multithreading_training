package com.studinskyi.metanit.readfiles_lambda;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ProjectUtilits {

    // установка текущего рабочего каталога
    public static String setWorkFolder(String strPathDirectory) {
        String strPathOfDefaultFolder = "c:\\test_QA\\"; // (по умолчанию c:\test_QA\)
        String workPathDirectory = "";

        if (!strPathDirectory.equals(""))
            workPathDirectory = strPathDirectory;
        else
            workPathDirectory = strPathOfDefaultFolder; // при пустом переданном пути назначать текущим каталог по умолчанию (по умолчанию c:\test_QA\)

        File fDirectory = new File(workPathDirectory);
        if (!fDirectory.exists()) {
            //myPath.mkdir();  //выбросит исключение, если родительского каталога нет в файловой системе
            fDirectory.mkdirs();  // создаст и всю цепочку каталгов если их нет.
            // проверка наличия созданной директории
            if (fDirectory.exists()) {
                System.out.println("directory was created: " + workPathDirectory);
            } else {
                // попытка создать каталог по умолчанию, если не был создан каталог по нужному пути
                System.out.println("Directory \"" + workPathDirectory + "\" did not created.");

                workPathDirectory = strPathOfDefaultFolder;
                fDirectory = new File(workPathDirectory);
                System.out.println("Trying to create a default folder \"" + strPathOfDefaultFolder + "\"");
                fDirectory.mkdirs();
            }
        }
        //else
        //System.out.println("directory is already exists: " + workPathDirectory);

        if (fDirectory.exists())
            System.out.println("Set current working directory - " + workPathDirectory);
        else
            // The working directory has not been defined
            System.out.println("The working directory was not found");

        return workPathDirectory;
    }

    public static List<File> listFilesInDirectory(String folderFiles) {
        System.out.println("list of files and directories:");
        List<File> listFiles = new LinkedList<File>();
        File dir = new File(folderFiles);

        if (dir.isDirectory()) {
            // если объект представляет каталог
            for (File item : dir.listFiles()) {
                // получаем все вложенные объекты в каталоге
                if (item.isDirectory()) {
                    System.out.println(folderFiles + item.getName() + "\tкаталог");
                } else {
                    listFiles.add(item);
                    System.out.println(folderFiles + item.getName() + "\tфайл");
                }
            }
        }
        return listFiles;
    }

    // существует ли данный файл или каталог по указанному пути
    public static boolean fileExist(String fullPathToFile) {
        Path path = Paths.get(fullPathToFile);
        if (Files.exists(path))
            return true;

        return false;
    }

    public static String getCurrentDateTimeString() {
        // для возможности последующего просмотра командой history
        Date d = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
        //FileManager.executedOperations.put(formatDate.format(d), FileManager.currentCommand);
        return formatDate.format(d);
    }

    public static String getCurrentDateTimeToNameFile() {
        Date d = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
        return formatDate.format(d);
    }
}

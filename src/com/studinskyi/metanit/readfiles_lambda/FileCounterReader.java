package com.studinskyi.metanit.readfiles_lambda;

import java.io.*;

public class FileCounterReader implements Runnable {
    private String fullPathToFile;
    private File file;
    private static volatile int countPositiveNumbers = 0;
    private PoolCounters poolCounters;
    private static volatile boolean stopCountingProcess = false;

    //public final ArrayList<Integer> listResult;

    public FileCounterReader(String fullPathToFile) {
        this.fullPathToFile = fullPathToFile;
       //this.poolCounters = poolResources;
        //this.listResult = new ArrayList<>();
    }

    public void run() {
        try {
            int sumNumbersOfFile = countNumbersFromFile();
            //collection.stream().flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new)
            //listResult.stream().map((x) -> x+x).forEach(System.out::println);
            //listResult.stream().mapToInt((s) -> Integer.parseInt(FileCounterReader::countNumbersFromFile)).toArray();
            System.out.println(Thread.currentThread().getName() + " : in the file " + fullPathToFile + " the sum of postitive nambers is = " + sumNumbersOfFile);
        } catch (IOException e) {
            System.out.println("error the operation run() for the thread: " + Thread.currentThread());
            e.printStackTrace();
        }
    }

    // синхронизированный метод для суммирования чисел из всех потоков в статическую переменную countPositiveNumbers
    public synchronized static void addPositiveNumber(int positiveNumber) {
        countPositiveNumbers = countPositiveNumbers + positiveNumber;
    }

    public int countNumbersFromFile() throws IOException {
        String strLine;
        String[] stringFragments;
        int tekNumb;
        int sumNumbers = 0;

        file = new File(fullPathToFile);
        if (!file.exists()) {
            System.out.println("file does not exist: " + fullPathToFile);
            return 0; // процесс обсчета отсутствующего файла прекращаем
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fullPathToFile));
            while ((strLine = reader.readLine()) != null && !stopCountingProcess) {
                stringFragments = strLine.split(" "); // разбиенте строки по разделителю в виде пробела
                for (String strFragment : stringFragments) {
                    // убираем все оставшиеся пробелы во фрагменте
                    strFragment = strFragment.replaceAll(" ", "");
                    // отсекаем случаи когда во фрагменте все пробелы (чтобы не получить при парсинге числа через parseInt ложные ошибки)
                    if (strFragment.equals(""))
                        continue; // переходим к обработке следующего фрагмента в строке

                    // если в другом потоке был выставлен флаг остановки, то следует прекратить обсчет и текущего потока
                    if (stopCountingProcess)
                        break; // завершаем проход по фрагментам строки

                    // выделение числа из фрагмента строки и его накопление в общем для потоков ресурсе
                    try {
                        //System.out.println("read fragment of text " + strFragment + " readig file " + fullPathToFile);
                        tekNumb = Integer.parseInt(strFragment);
                        if (tekNumb > 0 && tekNumb % 2 == 0) {
                            // накапливается сумма только положительных четных чисел
                            addPositiveNumber(tekNumb);
                            sumNumbers = sumNumbers + tekNumb;
                            System.out.println(Thread.currentThread().getName() + " : current sum of numbers: " + countPositiveNumbers);
                            //System.out.println(Thread.currentThread().getName() + " : current sum of numbers: " + countPositiveNumbers + " in moment " + getCurrentDateTimeString() + " in file " + fullPathToFile);
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("ERROR OF PARSE fragment " + strFragment + " from the readig file " + fullPathToFile);
                        stopCountingProcess = true;
                        break; // завершаем проход по фрагментам строки
                        //e.printStackTrace();
                    }
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("error opening file - FileNotFoundException, from path:" + fullPathToFile);
            //e.printStackTrace();
        } catch (IOException e) {
            System.out.println("error opening file - IOException, from path:" + fullPathToFile);
            //e.printStackTrace();
        }
        return sumNumbers; // сумма положительных чисел в данном файле (не относится к основной задаче)
    }

    public static int getCountPositiveNumbers() {
        return countPositiveNumbers;
    }

    public static boolean isStopCountingProcess() {
        return stopCountingProcess;
    }
}

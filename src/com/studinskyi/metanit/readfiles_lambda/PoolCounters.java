package com.studinskyi.metanit.readfiles_lambda;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

public class PoolCounters {
    private String fullPathDirectory = "";
    private List<File> listFiles;
    private static volatile boolean stopCountingProcess = false;
    private static volatile int countPositiveNumbers = 0;

    public PoolCounters(String fullPathDirectory) {
        this.fullPathDirectory = fullPathDirectory;
    }

    public void runAndSummingNumbersFromTreads() {
        listFiles = ProjectUtilits.listFilesInDirectory(fullPathDirectory);// получение списка путей к файлам из указанной папки
        System.out.println("Directory contains " + listFiles.size() + " files");
        // проход по всем файлам и запуск нитей обработки через лямбда-выражения
        listFiles.forEach(PoolCounters::readResourceLambda);
    }

    private static void readResourceLambda(File elemResource) {
        String pathToFile = elemResource.getPath();
        System.out.println("start counting process in file " + pathToFile);
        new Thread(() -> {
            try {
                Files.lines(Paths.get(pathToFile)).forEach(
                        (String strLine) -> {
                            if (strLine != null && !stopCountingProcess) {
                                //System.out.println(strLine);
                                addPositiveNumber(Arrays.stream(strLine.split(" ")).
                                        mapToInt(strFragment -> {
                                            strFragment = strFragment.replaceAll(" ", "");
                                            if(strFragment.equals(""))
                                                return 0;

                                            try {
                                                return Integer.parseInt(strFragment);
                                            } catch (NumberFormatException e) {
                                                setStopCountingProcess();
                                                return 0;
                                            }
                                        }).filter(chislo -> chislo > 0 && chislo % 2 == 0).sum());
                                System.out.println("current sum of numbers: " + countPositiveNumbers);

                            }
                        }
                );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    // синхронизированный метод для суммирования чисел из всех потоков в статическую переменную countPositiveNumbers
    public synchronized static void addPositiveNumber(int positiveNumber) {
        countPositiveNumbers = countPositiveNumbers + positiveNumber;
    }

    private void readResourceLambdaExecutor(File elemResource, ExecutorService serviceExecutor) {
        // для варианта с использованием Лямбда и ExecutorService
        System.out.println("start counting process in file " + elemResource.getPath());
        FileCounterReader counterReader = new FileCounterReader(elemResource.getPath());
        Runnable runnableThread = new Thread(counterReader);
        serviceExecutor.execute(runnableThread);
    }

    public synchronized static void setStopCountingProcess() {
        PoolCounters.stopCountingProcess = true;
    }


//    private void readResource(File elemResource, int numberOfResource) {
    //        // для варианта без использования ExecutorService
    //        // запуск ВСЕХ потоков параллельно
    //        System.out.println("start counting process in file " + numberOfResource + ": " + elemResource.getPath());
    //        FileCounterReader counterReader = new FileCounterReader(elemResource.getPath(), this);
    //        Thread thread = new Thread(counterReader, "thread " + numberOfResource);
    //        thread.start();
    //    }

    public List<File> getListFiles() {
        return listFiles;
    }
}

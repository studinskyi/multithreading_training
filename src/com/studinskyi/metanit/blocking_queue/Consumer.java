package com.studinskyi.metanit.blocking_queue;

public class Consumer implements Runnable {
    private final BlockingQueue<Integer> queue;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println("[Consumer] run");
        while (true) {
            try {
                consume();
                Thread.currentThread().sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void consume() throws InterruptedException {
        Integer i = queue.take();
        System.out.println("[Consumer] consumed: " + i);
    }
}
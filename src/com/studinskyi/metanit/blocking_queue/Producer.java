package com.studinskyi.metanit.blocking_queue;

import java.util.Random;

public class Producer implements Runnable {
    private final BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println(queue.toString() + "[Producer - " + Thread.currentThread().getName() + "] run");
        while (true) {
            try {
                queue.put(produce());
                Thread.currentThread().sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Integer produce() {
        Integer i = new Random().nextInt(100);
        System.out.println(queue.toString() + "[Producer - " + Thread.currentThread().getName() + "] produce: " + i);
        return i;
    }
}

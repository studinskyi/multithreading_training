package com.studinskyi.metanit.blocking_queue;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Integer> queue = new BlockingQueue<>(5);
        new Thread(new Producer(queue), "Prod 1").start();
        Thread.currentThread().sleep(1000);
        new Thread(new Consumer(queue), "Cons 1").start();
        new Thread(new Consumer(queue), "Cons 2").start();
    }

}

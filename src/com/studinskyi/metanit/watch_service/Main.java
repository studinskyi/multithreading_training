package com.studinskyi.metanit.watch_service;

import java.io.IOException;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) {

        // to create a WatchService instance using the java.nio.file.FileSystems class:
        WatchService watchService = null;
        try {
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //  have to create the path to the directory we want to monitor:
        Path path = Paths.get("c:\\test_QA\\");
        //        Files files = Files.getFileStore(path);

        //  we have to register the path with watch service
        try {
            path.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_DELETE,
                    StandardWatchEventKinds.ENTRY_MODIFY);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // watch key instance is removed from the watch service queue every time it is returned by a poll operation.
        // The reset API call puts it back in the queue to wait for more events.
        try {
            WatchKey key;
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    System.out.println(
                            "Event kind:" + event.kind()
                                    + ". File affected: " + event.context() + ".");
                }
                key.reset();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
